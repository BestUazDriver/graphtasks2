package com.company.graph2;

public class Edge implements Comparable<Edge> {
    private int startVertex;
    private int finalVertex;
    private int weight;

    public Edge(int startVertex, int finalVertex, int weight) {
        this.startVertex = startVertex;
        this.finalVertex = finalVertex;
        this.weight = weight;
    }

    public int getStartVertex() {
        return startVertex;
    }


    public int getFinalVertex() {
        return finalVertex;
    }



    public int getWeight() {
        return weight;
    }



    @Override
    public int compareTo(Edge e) {
        return this.getWeight() - e.getWeight();
    }
}

