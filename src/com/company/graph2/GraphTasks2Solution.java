package com.company.graph2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> neighbour = new HashMap<>();
        ArrayList<Integer> vertex = new ArrayList<>();
        ArrayList<Integer> usedNumbers = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {

            if (adjacencyMatrix[startIndex][i] > 0) {
                neighbour.put(i, adjacencyMatrix[startIndex][i]);
            }
        }
        usedNumbers.add(startIndex);
        vertex.addAll(neighbour.keySet());
        System.out.println(vertex.toString());
        while (usedNumbers.size() < adjacencyMatrix.length) {
            for (int i = 0; i < vertex.size(); i++) {
                for (int y = 0; y < adjacencyMatrix.length; y++) {
                    if (adjacencyMatrix[vertex.get(i)][y] > 0 && !usedNumbers.contains(y)) {
                        neighbour.put(y, 1000);
                    }
                    if (adjacencyMatrix[vertex.get(i)][y] > 0 && !usedNumbers.contains(y) && neighbour.get(y) > neighbour.get(i) + adjacencyMatrix[vertex.get(i)][y]) {
                        neighbour.put(y, neighbour.get(i) + adjacencyMatrix[vertex.get(i)][y]);
                        vertex.add(y);
                        System.out.println(neighbour.get(i));
                    }

                }
                usedNumbers.add(vertex.get(i));
            }
        }
        System.out.println(usedNumbers.toString());

        return neighbour;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int size = 0;
        int min = 1;
        int total = 0;
        int local = 0;
        int[][] matrix = new int[adjacencyMatrix.length][adjacencyMatrix.length];
        ArrayList<Integer> vertex = new ArrayList<>();

        for (int i = 1; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[0][i] > 0) {
                min = adjacencyMatrix[0][i];
                break;
            }
        }
        for (int i = 1; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[0][i] > 0 && adjacencyMatrix[0][i] < min) {
                min = adjacencyMatrix[0][i];
                System.out.println(min);
                local = i;

            }
        }
        matrix[0][local] = min;
        matrix[local][0] = min;
        size = size + min;
        vertex.add(local);
        while (vertex.size() < adjacencyMatrix.length) {
            min = 1000;
            for (int i = 0; i < vertex.size(); i++) {
                for (int y = 1; y < adjacencyMatrix.length; y++) {
                    if (adjacencyMatrix[vertex.get(i)][y] > 0 && adjacencyMatrix[vertex.get(i)][y] < min && !vertex.contains(y)) {
                        min = adjacencyMatrix[i][y];
                        total = y;
                        local = vertex.get(i);
                    }
                }
            }
            matrix[total][local] = adjacencyMatrix[total][local];
            matrix[local][total] = matrix[total][local];
            size = size + min;
            vertex.add(total);

        }
        size = size - matrix[0][0];
        matrix[0][0] = 0;
        System.out.println(size);
        System.out.println(vertex.toString());
        return size;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        ArrayList<Edge> edges = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = i; j < adjacencyMatrix[i].length; j++) {
                if (adjacencyMatrix[i][j] != 0) {
                    edges.add(new Edge(i, j, adjacencyMatrix[i][j]));
                }
            }
        }
        Collections.sort(edges);
        ArrayList<Integer> vertex = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            vertex.add(i);
        }
        int totalWeight = 0;
        for (Edge edge : edges) {
            int weight = edge.getWeight();
            int start = edge.getStartVertex();
            int end = edge.getFinalVertex();
            if (!vertex.get(start).equals(vertex.get(end))) {
                totalWeight = totalWeight + weight;
                int a = vertex.get(start);
                int b = vertex.get(end);
                for (int i = 0; i < adjacencyMatrix.length; i++) {
                    if (vertex.get(i) == b) {
                        vertex.set(i, a);
                    }
                }
            }
        }
        return totalWeight;
    }
}


